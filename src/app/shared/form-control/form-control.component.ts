import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-form-control',
    templateUrl: './form-control.component.html',
    styleUrls: ['./form-control.component.scss']
})
export class FormControlComponent implements OnInit {

    @Input()
    public id: string;

    @Input()
    public value: string;

    @Input()
    public label: string;

    @Input()
    public type: string;

    constructor() {
    }

    ngOnInit(): void {
    }

}
