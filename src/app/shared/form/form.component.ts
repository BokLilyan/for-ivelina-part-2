import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
    @Input()
    public id: string;

    @Input()
    public title: string;

    constructor() {
    }

    ngOnInit(): void {
    }

}
